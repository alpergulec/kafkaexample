package com.alper.kafka.resource;

import com.alper.kafka.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("kafka")
public class UserResource {

    @Autowired
    private KafkaTemplate<String, User> kafkaTemplate;
    private static final String TOPIC = "TEST";

    @GetMapping("/publish/{name}")
    public String post(@PathVariable("name") String name) {

        kafkaTemplate.send(TOPIC, new User(1,name,"Gulec"));
        return "Published succesfully";
    }

}
